/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
 */
package de.unikn.disy.hash;

import de.unikn.disy.hash.CRC32;
import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 *
 * @author zink
 */
public class CRC32Test extends HashFunctionTest {
    
    private static long crc1; //, crc2;
    
    static {
        java.util.zip.CRC32 crc = new java.util.zip.CRC32();
        crc.update(HashFunctionTest.key.getBytes());
        crc1 = crc.getValue();
        //crc.reset();
        //crc.update(de.unikn.disy.util.Convert.objectToBytes(HashFunctionTest.key));
        //crc2 = crc.getValue();
        crc = null;
    }
    
            
    public CRC32Test() {
        //super(new CRC32(), crc1, crc2);
    	super(new CRC32(), crc1);
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
}
