/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de
	
	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
 */
package de.unikn.disy.hash;

import java.util.Random;

/**
 * Implements a hash function as proposed by BUZ (Robert Uzgalis) [1]
 * The hash function is influenced by McKenzie's et al CBUHash. The goal of
 * the BUZHash is to calculate large hash values cheaper.
 * 
 * [1] http://www.serve.net/buz/hash.adt/java.000.html
 * 
 * @author zink
 */
public class BUZHash extends HashFunction {
    private static long[] table = new long[0xFF];
    
    public BUZHash() {
        this(System.nanoTime());
    }
    
    public BUZHash(final long seed) {
        initial = 0xe12398c6d9ae3b99L;
        Random r = new Random(seed);
        for (int i=0; i<0xFF; i++) table[i] = r.nextLong();
    }
    
    @Override
    public long hash(Long init, final byte[] b, int off, int len) {
        long hash = init;
        while (--len >= 0) {
            hash = (hash << 1) ^ (hash >>> 63) ^ table[(int)(b[off++] &0xff)];
        }
        return hash;
    } 
}
