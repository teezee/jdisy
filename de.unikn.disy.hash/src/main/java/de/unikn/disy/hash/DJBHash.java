/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
 */
package de.unikn.disy.hash;

/**
 * Implement a hash function proposed by Daniel J. Bernstein. aka BersteinHash
 * It's very suitable for English strings.
 * The original proposal computes the hash as
 *      hash = 33 * hash + b[i];
 * This leads the same result as the following implementation. Since
 * rotation and addition are generally more efficient than multiplication
 * it is used here.
 * 
 * @author zink
 */
public final class DJBHash extends HashFunction {
    protected long SEED = 5381L;
    
    public DJBHash() { initial = 5381L; }
    
    @Override
    public long hash(Long init, final byte[] b, int off, int len) {
        long hash = init;
        while (--len >= 0) {
            hash = ((hash << 5) + hash) + b[off++];
        }
        return hash;
    }
}
