/*
	Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de	
	
	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or
	sell copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
*/

package de.unikn.disy.util;

import java.io.IOException;
import java.util.Arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author zink
 */
public class ConverterTest {
    
    public ConverterTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of objectToBytes method, of class Convert.
     * @throws IOException 
     */
    @Test
    public void testObjectToBytes() throws IOException {
        System.out.println("objectToBytes");
        Object obj = "Hello World!";
        byte[] expResult = {(byte)0xac, (byte)0xed, (byte)0x0, (byte)0x5, (byte)0x74, (byte)0x0, (byte)0xc, (byte)0x48, (byte)0x65, (byte)0x6c, (byte)0x6c, (byte)0x6f, (byte)0x20, (byte)0x57, (byte)0x6f, (byte)0x72, (byte)0x6c, (byte)0x64, (byte)0x21};
        byte[] result = Convert.objectToBytes(obj);
        assertTrue(Arrays.equals(expResult, result));
    }

    /**
     * Test of bytesToObject method, of class Convert.
     * @throws ClassNotFoundException 
     * @throws IOException 
     */
    @Test
    public void testBytesToObject() throws IOException, ClassNotFoundException {
        System.out.println("bytesToObject");
        byte[] bytes = {(byte)0xac,(byte)0xed,(byte)0x0,(byte)0x5,(byte)0x74,(byte)0x0,(byte)0x8,(byte)0x48,(byte)0x65,(byte)0x6c,(byte)0x6c,(byte)0x6f,(byte)0x20,(byte)0x55,(byte)0x21};
        Object expResult = "Hello U!";
        Object result = Convert.bytesToObject(bytes);
        assertEquals(expResult, result);
    }

    /**
     * Test of shortToBytes method, of class Convert.
     */
    @Test
    public void testShortToBytes() {
        System.out.println("shortToBytes");
        Short val = (short)0xAFFE;
        byte[] expResult = {(byte)0xAF,(byte)0xFE};
        byte[] result = Convert.shortToBytes(val);
        assertTrue(Arrays.equals(expResult, result));
    }

    /**
     * Test of intToBytes method, of class Convert.
     */
    @Test
    public void testIntToBytes() {
        System.out.println("intToBytes");
        Integer val = 0xCAFEBABE;
        byte[] expResult = {(byte)0xCA, (byte)0xFE, (byte)0xBA,(byte)0xBE};
        byte[] result = Convert.intToBytes(val);
        assertTrue(Arrays.equals(expResult, result));
    }

    /**
     * Test of longToBytes method, of class Convert.
     */
    @Test
    public void testLongToBytes() {
        System.out.println("longToBytes");
        Long val = 0x48656c6c6f205521L;
        byte[] expResult = {(byte)0x48,(byte)0x65,(byte)0x6c,(byte)0x6c,(byte)0x6f,(byte)0x20,(byte)0x55,(byte)0x21};
        byte[] result = Convert.longToBytes(val);
        assertTrue(Arrays.equals(expResult, result));
    }

    /**
     * Test of floatToBytes method, of class Convert.
     */
    @Test
    public void testFloatToBytes() {
        System.out.println("floatToBytes");
        Float val = 1f * (float)Math.pow(2, 99) * 1.6368829f;
        byte[] expResult = {(byte)0x71,(byte)0x51,(byte)0x85,(byte)0x61};
        byte[] result = Convert.floatToBytes(val);
        assertTrue(Arrays.equals(expResult, result));
    }

    /**
     * Test of doubleToBytes method, of class Convert.
     */
    @Test
    public void testDoubleToBytes() {
        System.out.println("doubleToBytes");
        Double val = 1d * Math.pow(2, 313) * 1.0374977960d;
        byte[] expResult = {(byte)0x53,(byte)0x80,(byte)0x99,(byte)0x97,(byte)0x49,(byte)0xf7,(byte)0xdf,(byte)0x8f};
        byte[] result = Convert.doubleToBytes(val);
        assertTrue(Arrays.equals(expResult, result));
    }
}
